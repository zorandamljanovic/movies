/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.controller;

import com.movies.model.Category;
import com.movies.model.CategoryDao;
import com.movies.model.Movie;
import com.movies.model.MovieDao;
import com.movies.model.OrdersDao;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author zdy
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    MovieDao movieDao;

    @Autowired
    OrdersDao ordersDao;

    @RequestMapping("/")
    public String index() {
        return "admin/index";
    }

    @RequestMapping("/updatecategory")
    public String updateCategory(@RequestParam Integer id, @RequestParam String name, @RequestParam String description, ModelMap model) {
        Category selectedCategory = categoryDao.getById(id);
        selectedCategory.setName(name);
        selectedCategory.setDescription(description);
        categoryDao.update(selectedCategory);
        List<Category> categories = categoryDao.find();
        model.addAttribute("categories", categories);
        model.addAttribute("selectedCategory", selectedCategory);
        return "admin/categories";
    }

    @RequestMapping("/categories")
    public String categories(@RequestParam(required = false) Integer id, ModelMap model) {
        List<Category> categories = categoryDao.find();
        model.addAttribute("categories", categories);
        if (id != null) {
            Category selectedCategory = categoryDao.getById(id);
            model.addAttribute("selectedCategory", selectedCategory);
        }

        return "admin/categories";
    }

    @RequestMapping("/movies")
    public String movies(@RequestParam(defaultValue = "1") Integer page, ModelMap model) {
        List<Movie> movies = movieDao.findByPage(page - 1);
        model.addAttribute("movies", movies);
        model.addAttribute("totalpages", movieDao.pages());
        return "admin/movies";
    }

    @RequestMapping(value = "/updatemovie", method = RequestMethod.GET)
    public String updatemovie(@RequestParam Integer id, ModelMap model) {
        List<Category> categories = categoryDao.find();
        model.addAttribute("categories", categories);
        Movie movie = movieDao.getById(id);
        model.addAttribute("movie", movie);
        return "admin/updatemovie";
    }

    @RequestMapping(value = "/updatemovie", method = RequestMethod.POST)
    public String updatemoviePost(@RequestParam Integer category, @RequestParam String title, @RequestParam BigDecimal price, @RequestParam Integer id, ModelMap model, HttpServletRequest request, @RequestParam MultipartFile photo) throws FileNotFoundException, IOException {
        Movie movie = movieDao.getById(id);
        List<Category> categories = categoryDao.find();
        model.addAttribute("categories", categories);
        if (photo != null && !photo.isEmpty()) {
            String filePath = request.getServletContext().getRealPath("resources/images");
            FileOutputStream fos = new FileOutputStream(filePath + "/" + photo.getOriginalFilename());
            movie.setPhoto(photo.getOriginalFilename());
            fos.write(photo.getBytes());
            fos.close();
        }
        
        movie.setTitle(title);
        movie.setPrice(price);
        movie.setCategory(category);
        movieDao.update(movie);
        model.addAttribute("movie", movie);
        return "admin/updatemovie";
    }
}
